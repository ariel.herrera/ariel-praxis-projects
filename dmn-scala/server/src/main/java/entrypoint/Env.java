package entrypoint;

import py4j.GatewayServer;

public class Env{
    /**
     * Initializing java environment for python execution and data binding
     * @param args
     */
    public static void main(String[] args) {
        Env app = new Env();
        // app is now the gateway.entry_point
        GatewayServer server = new GatewayServer(app, 27125);
        server.start();
    }
}