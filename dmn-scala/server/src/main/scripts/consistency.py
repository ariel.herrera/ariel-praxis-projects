from sqlalchemy import create_engine
from sqlalchemy import text
from py4j.java_gateway import JavaGateway
from py4j.java_gateway import GatewayParameters

## Query that extract variable information at tasks scope
task_varinst_query = text(
    "select ahv.name_ as var_name_, " +
    "ahv.task_id_ as task_id_, " +
    "agb.id_ as obj_id_, agb.bytes_ as bytes_ " +
    "from act_hi_varinst ahv " +
    "inner join act_ge_bytearray agb on ahv.bytearray_id_ = agb.id_ " +
    "where ahv.task_id_ = :task_id_ AND " +
    "ahv.name_ = :var_name_ " +
    "order by agb.id_ desc"
)

## Query that extract variable information at processes scope
process_varinst_query = text(
    "select ahv.name_ as var_name_, " +
    "ahv.proc_inst_id_ as proc_id_, " +
    "agb.id_ as obj_id_, agb.bytes_ as bytes_ " +
    "from act_hi_varinst ahv " +
    "inner join act_ge_bytearray agb on ahv.bytearray_id_ = agb.id_ " +
    "where ahv.proc_inst_id_ = :proc_id_ AND " +
    "ahv.name_ = :var_name_ " +
    "order by agb.id_ desc"
)

## Operation to update variables in CamundaBPMN-Engine content-variables table
varinst_update = text(
    "update act_ge_bytearray " +
    "set bytes_ = :binary_data_ " +
    "where id_ = :binary_data_id_ "
)

## Function that extracts and deserializes the variables content
def get_varinst(db, gateway, varinst_scoped_query, parameters):
    var_inst = db.execute(
        varinst_scoped_query,
        parameters
    ).fetchone()
    deserialized_row = dict(var_inst)
    raw_data = deserialized_row["bytes_"].tobytes()
    stream_raw = gateway.jvm.java.io.ByteArrayInputStream(raw_data)
    stream_obj = gateway.jvm.java.io.ObjectInputStream(stream_raw)
    raw_obj = stream_obj.readObject()
    deserialized_row["bytes_"] = raw_obj

    return deserialized_row

## Function that serializes the variables content
def convert_to_bytes(gateway, deserialized_row):
    var_list = gateway.jvm.java.util.ArrayList()
    jvm_changing_var = gateway.jvm.java.util.LinkedHashMap()
    for i in deserialized_row:
        for token, entry in i.items():
            jvm_changing_var.put(token, entry)
        var_list.add(jvm_changing_var)

    stream_raw = gateway.jvm.java.io.ByteArrayOutputStream()
    stream_obj = gateway.jvm.java.io.ObjectOutputStream(stream_raw)
    stream_obj.writeObject(var_list)
    return stream_raw.toByteArray()


def update_varinst(db, varinst_scoped_update, parameters):
    db.execute(
        varinst_scoped_update,
        parameters
    )

## Function that updatees the variables content
def appendingOnVarEntry(deserialized_data):
    deserialized_data.append(deserialized_data["bytes_"][0])
    deserialized_data["bytes_"][0]["task_name"] = "Adjuntar requisitos"
    return deserialized_data

## Function that verifies that there isn't duplicated uuids
def validateUuidUniqueness(deserialized_data):
    result = dict()
    for value in deserialized_data:
        if not value["uuid"] in result:
            result.update({value["uuid"]:value})
    return list(result.values())

## Function that defines extracts, modifies and updates variables data in a defined scope
def ETL_like_processing(db, gateway, varinst_query, scoped_parameters, varinst_op):
    ## Extracting
    deserialized_data = get_varinst(db, gateway, varinst_query, scoped_parameters)
    ## variable operations
    ## Transforming
    transformed_data = varinst_op(deserialized_data["bytes_"])
    binary_updated_data = convert_to_bytes(gateway, transformed_data)
    ## Loading
    update_varinst(db, varinst_update, {"binary_data_": binary_updated_data, "binary_data_id_": deserialized_data["obj_id_"]})

## Initilizing database connection session
db = create_engine("postgres://admin:admin@localhost:5433/metro")
## Connecting to JVM environment server
gateway = JavaGateway(gateway_parameters=GatewayParameters(port=27125))
##Task scope
ETL_like_processing(db, gateway, task_varinst_query, {"task_id_": '9627', "var_name_": 'requirementsDocuments'}, validateUuidUniqueness)
##Process scope
ETL_like_processing(db, gateway, process_varinst_query,  {"proc_id_": '9610', "var_name_": 'requirementsDocuments'}, validateUuidUniqueness)
## Closing connection
db.dispose()

