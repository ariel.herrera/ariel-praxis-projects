from sqlalchemy import create_engine
from sqlalchemy import text
from py4j.java_gateway import JavaGateway
from py4j.java_gateway import get_field
from py4j.java_gateway import GatewayParameters
import requests
import json

## Query that extract variable information at processes scope
process_varinst_query = text(
    "select ahv.name_ as var_name_, " +
    "ahv.proc_inst_id_ as proc_id_, " +
    "agb.id_ as obj_id_, agb.bytes_ as bytes_ " +
    "from act_hi_varinst ahv " +
    "inner join act_ge_bytearray agb on ahv.bytearray_id_ = agb.id_ " +
    "where ahv.proc_inst_id_ = :proc_id_ " +
    "order by agb.id_ desc"
)

process_inst_query = text(
    "select atp.start_time_, mup.id, mup.name, mup.email " +
    "from act_hi_procinst atp " +
    "inner join act_hi_taskinst att " +
    "on atp.id_ = att.proc_inst_id_ " +
    "inner join metro_user_profiles mup " +
    "on att.assignee_ = cast(mup.id as varchar(255)) " +
    "where atp.id_ = :proc_id_ and att.id_ = :task_id_"
)


def formatting_multi_dim_varinst(var_inst):
    result = dict()
    for variable in var_inst:
        deserialized_row = dict(variable)
        raw_data = deserialized_row["bytes_"].tobytes()
        stream_raw = gateway.jvm.java.io.ByteArrayInputStream(raw_data)
        stream_obj = gateway.jvm.java.io.ObjectInputStream(stream_raw)
        raw_obj = stream_obj.readObject()
        result.update({deserialized_row["var_name_"]: raw_obj})
    return result

def formatting_date_varinst(var_inst):
    variable = var_inst[0]
    deserialized_row = dict(variable)
    return deserialized_row["start_time_"], {"id": deserialized_row["id"], "name": deserialized_row["name"], "email": deserialized_row["email"]}

def get_varinst(db, query, parameters, formating_op):
    var_inst = db.execute(
        query,
        parameters
    ).fetchall()
    return formating_op(var_inst)

def get_proc_inst_start_date(db, query, parameters, formating_op):
    return get_varinst(db, query, parameters, formating_op)

gateway = JavaGateway(gateway_parameters=GatewayParameters(port=27125, auto_field=True))

db = create_engine("postgres://admin:admin@192.168.1.164:5433/metroprod")
deserialized_data = get_varinst(db, process_varinst_query, {"proc_id_": '2525436'}, formatting_multi_dim_varinst)

#process_inst_data = get_field(deserialized_data,'deserialized_data')


process_start_time, eng_assignee_data = get_proc_inst_start_date(db, process_inst_query, {"proc_id_": '2525436', "task_id_": '2638322'}, formatting_date_varinst)
deserialized_data["processStartDate"] = process_start_time.isoformat()

deserialized_data["engineeringUser"] = eng_assignee_data

headers = {
    'Content-Type': "application/json",
    'X-Auth-Token': "eyJhbGciOiJIUzUxMiJ9.eyJpZCI6NDMsIm5hbWUiOiJzYXJhcGlxdWkiLCJzdWIiOiI0MyIsImlhdCI6MTU3MDYzOTExNSwiZXhwIjoxNTcwNjY3OTE1fQ.GWXwXia7bnzSb_rZ0mAnFWWJRPQfgeh_s4419l5VXLQpN8GNp0vT01g88-kBn7dYfRREH8muWkR26AcKPJGpcQ"
}

conn = requests.request(
    "PUT",
    str.format("http://localhost:8082/metro/sarapiqui/processes/generate/{0}", "rejectionMinute"),
    json = str(deserialized_data).replace("'", "\"").replace("None", "null"),
    headers = headers
)

print(conn.status_code)

print(conn.reason)

print(conn.text)

conn.close()