from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.contrib.operators.ssh_operator import SSHHook
from datetime import datetime, timedelta


default_args = {
    'owner': 'ariel',
    'depends_on_past': False,
    'start_date': datetime(2019, 7, 1),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
    # 'queue': 'bash_queue',
    'pool': 'Testing_Pool',
    # 'priority_weight': 10,
    # 'end_date': datetime(2016, 1, 1),
}

dag = DAG('automation', default_args=default_args, schedule_interval=timedelta(days=0))

# t1, t2 and t3 are examples of tasks created by instantiating operators

sshHook = SSHHook(ssh_conn_id="ssh_host_conn")

t0 = SSHOperator(
    command="cd /home/ariel/workspace/msa/alkaid-metro && docker-compose -p msa up -d",
    ssh_hook=sshHook,
    task_id='load_msa',
    retries=3,
    dag=dag
)

t1 = BashOperator(
    task_id='print_date',
    bash_command= "`date >> /tmp/a.out`",
    dag=dag
)

t2 = SSHOperator(
    command="cd /home/ariel/workspace/msa/alkaid-metro && docker-compose -p msa stop",
    ssh_hook=sshHook,
    task_id='stop_msa',
    retries=3,
    dag=dag
)

t3 = BashOperator(
    task_id='print_ls',
    bash_command="`ls -la /tmp >> /tmp/a.out`",
    dag=dag
)

t1.set_upstream(t0)
t2.set_upstream(t1)
t3.set_upstream(t2)