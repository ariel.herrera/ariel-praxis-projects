import ballerina/http;
import ballerina/log;

http:LoadBalanceClient lbBackendEPWP = new({

        targets: [
            { url: "http://bal:9000/mock1" },
            { url: "http://bal:9000/mock2" }
        ],
        timeoutMillis: 6000
});

@http:ServiceConfig {
    basePath: "/lb"
}
service loadBalancerDemoService on new http:Listener (9010) {

    @http:ResourceConfig {
        methods: ["GET"],
        path: "/"
    }

    resource function connectPOSTWithoutParams(http:Caller caller, http:Request req) {
        json requestPayload = {};

        var response = lbBackendEPWP->get("/");

        loadBalancerDemoService.responseProcessing(response, caller);
    }

    function responseProcessing(http:Response|error response, http:Caller caller){
        if (response is http:Response) {
            var responseToCaller = caller->respond(response);
            if (responseToCaller is error) {
                log:printError("Error sending response",
                                err = responseToCaller);
            }
        } else {
            http:Response outResponse = new;
            outResponse.statusCode = 500;
            outResponse.setPayload(<string>response.detail().message);
            var responseToCaller = caller->respond(outResponse);
            if (responseToCaller is error) {
                log:printError("Error sending response", err = responseToCaller);
            }
        }
    }

    @http:ResourceConfig {
        methods: ["GET"],
        path: "/"
    }

    resource function ListBehavior(http:Caller caller, http:Request req) {
        json requestPayload = {};

        var response = lbBackendEPWP->get("/");

        loadBalancerDemoService.responseProcessing(response, caller);
    }
}