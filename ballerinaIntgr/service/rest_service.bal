import ballerina/http;
import ballerina/log;
import ballerina/runtime;

listener http:Listener backendEP = new(9000);

@http:ServiceConfig {
    basePath: "/mock1"
}
service mock1 on backendEP {
    @http:ResourceConfig {
        methods: ["GET"],
        path: "/"
    }
    resource function mock1Resource(http:Caller caller, http:Request req) {
        var pid = runtime:getInvocationContext().id;
        var msg = "Mock1 resource was invoked." + pid;
        log:printTrace(msg);
        var responseToCaller = caller->respond(msg);
        if (responseToCaller is error) {
            log:printError("Error sending response from mock service", err = responseToCaller);
        }
    }
}

@http:ServiceConfig {
    basePath: "/mock2"
}
service mock2 on backendEP {
    
    @http:ResourceConfig {
        methods: ["GET"],
        path: "/"
    }
    resource function mock2Resource(http:Caller caller, http:Request req) {
        
        var responseToCaller = caller->respond("Mock2 resource was invoked.");
        if (responseToCaller is error) {
            log:printError("Error sending response from mock service", err = responseToCaller);
        }
    }
}
